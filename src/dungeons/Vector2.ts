export class Vector2 {
	x: number;
	y: number;
	isReadonly: boolean = false;

	constructor();
	constructor(x: number, y: number);
	constructor(x?: number, y?: number) {
		this.x = x || 0;
		this.y = y || 0;
	}

	immutable(): Vector2 {
		this.isReadonly = true;
		return this;
	}

	clone(): Vector2 {
		return new Vector2(this.x, this.y);
	}

	floor(): Vector2 {
		console.assert(!this.isReadonly);
		this.x = Math.floor(this.x);
		this.y = Math.floor(this.y);
		return this;
	}

	round(): Vector2 {
		console.assert(!this.isReadonly);
		this.x = Math.floor(this.x);
		this.y = Math.floor(this.y);
		return this;
	}

	add(other: Vector2): Vector2 {
		console.assert(!this.isReadonly);
		this.x += other.x;
		this.y += other.y;
		return this;
	}

	subtract(other: Vector2): Vector2 {
		console.assert(!this.isReadonly);
		this.x -= other.x;
		this.y -= other.y;
		return this;
	}

	multiply(scalar: number): Vector2 {
		console.assert(!this.isReadonly);
		this.x *= scalar;
		this.y *= scalar;
		return this;
	}

	divide(scalar: number): Vector2 {
		console.assert(!this.isReadonly);
		this.x /= scalar;
		this.y /= scalar;
		return this;
	}

	normalise(): Vector2 {
		return this.divide(this.length());
	}

	apply(func: (x: number) => number) {
		console.assert(!this.isReadonly);
		this.x = func(this.x);
		this.y = func(this.y);
		return this;
	}

	sqdist(pos: Vector2): number {
		const delta = this.clone().subtract(pos);
		return delta.x * delta.x + delta.y * delta.y;
	}

	length(): number {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}

	hash(): number {
		return this.x + this.y * 10000;
	}

	equals(other: Vector2) {
		return this.x == other.x && this.y == other.y;
	}

	toString(): string {
		return `${this.x},${this.y}`;
	}
}
