import { Map } from "./Map";
import { Graph, Node } from "./Graph";
import { Vector2 } from "./Vector2";
import { Rect2 } from "./Rect2";
import { PRNGRandom, assert, } from "./utils";

let rng : PRNGRandom;
let delaunayGraph: Graph;

function grow(graph: Graph, start: Node, width: number, height: number): Node[] {
	const bounds = graph.getBounds();
	assert(bounds.size.x < width - 2 && bounds.size.y < height - 2);
	assert(graph.getNodes().length < 100);

	const room = start.bounds;

	const LUT = [ 0, 0, 1, 2, 2, 2, 2, 3, 3 ];
	const children = rng.randomElement(LUT);
	if (children == 0) {
		return;
	}

	if (graph.getNodes().filter(node => node.bounds.name).length + children > 20) {
		return;
	}

	const directions = [
		new Vector2( 0, -1).immutable(),
		new Vector2( 0,  1).immutable(),
		new Vector2(-1,  0).immutable(),
		new Vector2( 1,  0).immutable(),
	];

	let newNodes: Node[] = [];

	let junction: Node = null;

	function attemptPlace(origin: Vector2, perpendicular: Vector2, size: Vector2, dist: number) {
		const offset = perpendicular.clone().multiply(dist)
		const pos = origin.clone().add(offset);
		const room = new Rect2(pos, size, `Room ${graph.getNodeCount()}`).round();
		room.center = pos.add(offset.clone().multiply(0.5)).round();

		const bounds = graph.getBounds().clone();
		bounds.extend(room);

		if (bounds.size.x >= width - 2 || bounds.size.y >= height - 2) {
			return;
		}

		assert(bounds.size.x < width - 2 && bounds.size.y < height - 2);

		if (graph.anyNodesIntersect(room.grow(1))) {
			return;
		}

		if (junction == null) {
			const junctionRect = new Rect2(origin.clone().round(), new Vector2(1, 1));
			if (rng.random() > 0.2 && !graph.anyNodesIntersect(junctionRect)) {
				junction = graph.addRectNode(junctionRect);
				graph.connect(start, junction);
			} else {
				junction = start;
			}
		}

		const node = graph.addRectNode(room);
		graph.connect(junction, node);
		newNodes.push(node);
	}

	while (directions.length > 0 && newNodes.length == 0) {
		const idx = Math.floor(rng.random() * directions.length);
		const direction = directions.splice(idx, 1)[0];

		const origin = room.sideFromDirection(direction).add(direction.clone().multiply(rng.randomInt(3, 6))).immutable();
		const roomSize = rng.randomSize(11, 2.5);

		const dist = rng.randomInt(2, 6);

		if (children == 1) {
			attemptPlace(origin, direction, roomSize, 1);
			continue;
		}

		const perpendicular = new Vector2(-direction.y, direction.x).immutable();
		attemptPlace(origin, perpendicular, roomSize, dist);
		attemptPlace(origin, perpendicular, roomSize, -dist);
		if (newNodes.length != children) {
			attemptPlace(origin, direction, roomSize, dist);
		}
	}

	newNodes.forEach(node => grow(graph, node, width, height));
}

export function generateGraph(width: number, height: number): Graph {
	let graph = new Graph();

	const entrance = new Rect2(new Vector2(25, 25), rng.randomSize(11, 2.5), "Entrance");

	const entranceNode = graph.addRectNode(entrance, null);
	grow(graph, entranceNode, width, height);

	const numberOfRooms = graph.getNodes().filter(node => node.bounds.name).length;
	if (numberOfRooms < 5) {
		return null;
	}

	graph.reposition(width, height);

	delaunayGraph = graph.delaunay();

	const delEdges = delaunayGraph.getEdges();
	let budget = delEdges.length * (rng.random() * 0.2 + 0.1);
	while (budget > 0) {
		const edge = rng.randomElement(delEdges);
		if (edge.weight < 400) {
			graph.connect(edge.from, edge.to);
			budget--;
		} else {
			budget -= 0.25;
		}
	}

	return graph;
}

export function growingTreeGenerator(map: Map, seed: string) {
	rng = new PRNGRandom(seed);

	let graph = null;
	while (graph == null) {
		graph = generateGraph(map.width, map.height);
	}

	const rooms = graph.getNodes().map(node => node.bounds).filter(rect => rect.name);
	rooms.forEach(room => {
		map.debug.addLabel(room);
		map.fillFloor(room, 1);
		map.placeWallRect(room, 1);
	});


	map.debug.addGraph(delaunayGraph);
	map.debug.addGraph(graph);

	map.placeHallways(graph, rooms);
}
