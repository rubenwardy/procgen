import { Vector2 } from "./Vector2";

export class Rect2 {
	readonly name: string;
	minPos: Vector2;
	size: Vector2;

	get maxPos(): Vector2 {
		return this.minPos.clone().add(this.size);
	}

	get center(): Vector2 {
		return this.size.clone().divide(2).add(this.minPos);
	}

	set center(v: Vector2) {
		this.minPos = v.clone().subtract(this.size.clone().divide(2)).round();
	}

	get top(): number {
		return this.minPos.y;
	}

	get right(): number {
		return this.maxPos.x;
	}

	get bottom(): number {
		return this.maxPos.y;
	}

	get left(): number {
		return this.minPos.x;
	}

	constructor(minPos: Vector2, size: Vector2, name?: string) {
		this.minPos = minPos || new Vector2();
		this.size = size || new Vector2();
		this.name = name;
	}

	clone(): Rect2 {
		return new Rect2(this.minPos.clone(), this.size.clone(), this.name);
	}

	floor(): Rect2 {
		return new Rect2(this.minPos.clone().floor(), this.size.clone().floor(), this.name);
	}

	round(): Rect2 {
		return new Rect2(this.minPos.clone().round(), this.size.clone().round(), this.name);
	}

	grow(v: number): Rect2 {
		return new Rect2(
			this.minPos.clone().subtract(new Vector2(v, v)),
			this.size.clone().add(new Vector2(v*2, v*2)))
	}

	extend(other: Rect2): Rect2 {
		const maxPos = new Vector2(
			Math.max(this.maxPos.x, other.maxPos.x),
			Math.max(this.maxPos.y, other.maxPos.y));

		this.minPos.x = Math.min(this.minPos.x, other.minPos.x);
		this.minPos.y = Math.min(this.minPos.y, other.minPos.y);
		this.size = maxPos.subtract(this.minPos);

		return this;
	}

	contains(p: Vector2): boolean {
		const minPos = this.minPos;
		const maxPos = this.maxPos;
		return p.x >= minPos.x && p.x < maxPos.x &&
				p.y >= minPos.y && p.y < maxPos.y;
	}

	intersects(b: Rect2): boolean {
		// const minPos = this.minPos;
		// const maxPos = this.maxPos;
		// const omaxPos = other.maxPos;
		// const interLeft = Math.max(minPos.x, other.minPos.x);
		// const interTop = Math.max(minPos.y, other.minPos.y);
		// const interRight = Math.min(maxPos.x, omaxPos.x);
		// const interBottom = Math.min(maxPos.y, omaxPos.y);
		// return interLeft < interRight && interTop < interBottom;
		const a = this;
		return a.left <= b.right &&
			b.left <= a.right &&
			a.top <= b.bottom &&
			b.top <= a.bottom;
	}

	toString(): string {
		return `${this.minPos.toString()} to ${this.maxPos.toString()}`;
	}

	sideFromDirection(direction: Vector2): Vector2 {
		const side = new Vector2();

		if (direction.x > 0.1) {
			side.x = this.maxPos.x - 1;
		} else if (direction.x < -0.1) {
			side.x = this.minPos.x;
		} else {
			side.x = this.center.x;
		}

		if (direction.y > 0.1) {
			side.y = this.maxPos.y - 1;
		} else if (direction.y < -0.1) {
			side.y = this.minPos.y;
		} else {
			side.y = this.center.y;
		}

		return side;
	}
};
