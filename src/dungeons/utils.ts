import { prng } from "seedrandom";
import { Vector2 } from "./Vector2";

export function createArray<T>(n: number, v: T) {
	let ret = new Array<T>(n);
	ret.fill(v, 0, n);
	return ret;
}

export function assert(cond) {
	if (!cond) {
		throw "Assertion failed";
	}
}

const seedrandom = require('seedrandom');

export class PRNGRandom {
	private rng: prng;

	constructor(seed: string) {
		this.rng = seedrandom(seed);
	}

	getRandomPointInCircle(pos: Vector2, radius: Vector2): Vector2 {
		const t = 2 * Math.PI * this.rng()
		const u = this.rng() + this.rng()
		let r = (u > 1) ? 2 - u : u;
		return new Vector2(radius.x * r * Math.cos(t), radius.y * r * Math.sin(t)).add(pos);
	}

	random(): number {
		return this.rng();
	}

	/** Random integer within inclusive range
	 *
	 * @param min Min, inclusive
	 * @param max Max, inclusive
	 */
	randomInt(min: number, max: number) {
		return Math.round(this.rng() * (max - min) + min);
	}

	randomElement<T>(array: T[]): T {
		return array[Math.floor(this.rng() * array.length)];
	}

	normal(): number {
		const samples = 5;

		var r = 0;
		for(var i = 0; i < samples; i++) {
			r += this.rng();
		}
		return r / samples;
	}

	randomSize(baseMax: number, ratioMax: number): Vector2 {
		const x = this.normal() * (baseMax - 2) + 2;
		const y = this.normal() * (baseMax - 2) + 2;
		return new Vector2(x, y).round();
	}
}
