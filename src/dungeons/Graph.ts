import { Vector2 } from "./Vector2";
import Delaunator from 'delaunator';
import TinyQueue from 'tinyqueue';
import { Rect2 } from "./Rect2";

export class Node {
	readonly id: number;
	readonly bounds: Rect2;

	get pos(): Vector2 {
		return this.bounds.center;
	}

	constructor(id: number, bounds: Rect2) {
		this.id = id;
		this.bounds = bounds;
	}
}

export class Edge {
	readonly from: Node;
	readonly to: Node;
	readonly weight: number;

	constructor(from: Node, to: Node, weight: number) {
		this.from = from;
		this.to = to;
		this.weight = weight;
	}
}

export class Graph {
	private id_counter = 0;
	private nodes = new Map<number, Node>();
	private edges = new Map<number, Set<number>>();
	private bounds : Rect2;

	addRectNode(rect: Rect2, id?: number): Node {
		if (id == null) {
			this.id_counter++;
			id = this.id_counter;
		} else {
			this.id_counter = Math.max(this.id_counter, id + 1);
		}

		let node = new Node(id, rect.clone());
		this.nodes.set(id, node);

		if (this.bounds == null) {
			this.bounds = new Rect2(node.bounds.minPos.clone(), node.bounds.size.clone());
		} else {
			this.bounds.extend(node.bounds);
		}

		return node;
	}

	getNodes(): Node[] {
		return Array.from(this.nodes.values());
	}

	getNode(id: number): Node {
		return this.nodes.get(id);
	}

	getNodeCount(): number {
		return this.nodes.size;
	}

	getBounds(): Rect2 {
		if (this.bounds == null) {
			let minPos = new Vector2(10000000, 10000000);
			let maxPos = new Vector2();

			this.getNodes().forEach(node => {
				minPos.x = Math.min(minPos.x, node.bounds.minPos.x);
				minPos.y = Math.min(minPos.y, node.bounds.minPos.y);

				maxPos.x = Math.max(maxPos.x, node.bounds.maxPos.x);
				maxPos.y = Math.max(maxPos.y, node.bounds.maxPos.y);
			});

			this.bounds = new Rect2(minPos, maxPos.clone().subtract(minPos));
		}

		return this.bounds;
	}

	getNodesFrom(node: Node): Node[] {
		const edges = this.edges.get(node.id) || [];
		return Array.from(edges).map(id => this.getNode(id));
	}

	getEdgesFrom(from: Node): Edge[] {
		const edges = this.edges.get(from.id) || [];
		return Array.from(edges).map(toi => {
			const to = this.getNode(toi)
			return new Edge(from, to, from.pos.sqdist(to.pos));
		});
	}

	getEdges(): Edge[] {
		let retval: Edge[] = []
		this.edges.forEach((tos: Set<number>, fromi: number) => {
			tos.forEach(toi => {
				const from = this.getNode(fromi);
				const to = this.getNode(toi)
				retval.push(new Edge(from, to, from.pos.sqdist(to.pos)));
			})
		})
		return retval;
	}

	hasDirectedEdge(from: Node, to: Node): boolean {
		const edges = this.edges.get(from.id);
		if (edges == null) {
			return false;
		}

		return edges.has(to.id);
	}

	hasUndirectedEdge(one: Node, two: Node): boolean {
		return this.hasDirectedEdge(one, two) && this.hasDirectedEdge(two, one);
	}

	connectDirected(from: Node, to: Node) {
		let edges = this.edges.get(from.id) || new Set<number>();
		this.edges.set(from.id, edges);

		edges.add(to.id);
	}

	connect(a: Node, b: Node) {
		this.connectDirected(a, b);
		this.connectDirected(b, a);
	}

	disconnectDirected(from: Node, to: Node) {
		let edges = this.edges.get(from.id) || new Set<number>();
		edges.delete(to.id);
	}

	disconnect(a: Node, b: Node) {
		this.disconnectDirected(a, b);
		this.disconnectDirected(b, a);
	}

	clearEdges() {
		this.edges.clear();
	}

	clone(): Graph {
		let graph = new Graph();
		this.nodes.forEach(node => graph.addRectNode(node.bounds, node.id));
		this.getEdges().forEach(edge => graph.connectDirected(edge.from, edge.to));
		return graph;
	}

	delaunay(): Graph {
		let graph = this.clone();
		graph.clearEdges();

		const nodes = graph.getNodes();
		const triangles = Delaunator.from(nodes.map(x => [x.pos.x, x.pos.y])).triangles;
		for (let i = 0; i < triangles.length; i += 3) {
			const a = nodes[triangles[i]];
			const b = nodes[triangles[i + 1]];
			const c = nodes[triangles[i + 2]];
			graph.connect(a, b);
			graph.connect(b, c);
			graph.connect(c, a);
		}
		return graph;
	}

	minimumSpanningTree(): Graph {
		let graph = this.clone();
		graph.clearEdges();

		const nodes = this.getNodes();
		if (nodes.length == 0) {
			return graph;
		}

		let edgeQueue = new TinyQueue<Edge>([], (a, b) => a[2] - b[2]);
		let explored = new Set<Node>();

		{
			const start = nodes[0];
			explored.add(start);
			this.getEdgesFrom(start).forEach(edge => edgeQueue.push(edge));
		}

		while (edgeQueue.length != 0) {
			const next: Edge = edgeQueue.pop();
			if (explored.has(next.to)) {
				continue;
			}

			graph.connect(next.from, next.to);
			this.getEdgesFrom(next.to).forEach(edge => edgeQueue.push(edge));
			explored.add(next.to);
		}

		return graph;
	}

	getNodesTouchingRect(rect: Rect2): Node[] {
		return Array.from(this.nodes.values()).filter(node => node.bounds.intersects(rect));
	}

	anyNodesIntersect(rect: Rect2): boolean {
		return Array.from(this.nodes.values()).some(node => node.bounds.intersects(rect));
	}

	relax(): Graph {
		console.log("Relaxing graph");
		let graph = this.clone();

		Array.from(graph.nodes.values()).map(node => {
			let force = new Vector2();
			let counter = 0;

			this.getEdgesFrom(node).forEach(edge => {
				let delta = edge.to.pos.clone().subtract(edge.from.pos);
				const fromRect = edge.from.bounds;
				const toRect = edge.to.bounds;
				const biggerFromRect = new Rect2(
					fromRect.minPos.clone().add(new Vector2(-2, -2)),
					fromRect.size.clone().add(new Vector2(4, 4)));

				if (biggerFromRect.intersects(toRect)) {
					console.log(` - ${fromRect.name} collides with ${toRect.name}`);
					delta.normalise().multiply(-20);
				} else {
					if (Math.abs(delta.x) < 20) {// && Math.abs(delta.y) < 40) {
						delta.x = -Math.sign(delta.x) * (20 - Math.abs(delta.x)) / 3;
					}

					if (Math.abs(delta.y) < 20) { // && Math.abs(delta.x) < 40) {
						delta.y = -Math.sign(delta.y) * (20 - Math.abs(delta.y)) / 3;
					}
				}

				force.multiply(counter).add(delta).divide(counter + 1);
				counter++;
			})

			return {
				node: node,
				force: force
			}
		}).forEach(pair => {
			pair.node.bounds.minPos.add(pair.force.multiply(0.1));
		});

		return graph;
	}

	reposition(width: number, height: number) {
		let bounds = this.getBounds();
		let target = new Vector2(width / 2, height / 2).subtract(bounds.size.clone().divide(2)).floor();
		let offset = target.clone().subtract(bounds.minPos);

		this.getNodes().forEach(node => {
			node.bounds.minPos.add(offset).round();
		});
	}
}
