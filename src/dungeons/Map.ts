import { Vector2 } from "./Vector2";
import { Rect2 } from "./Rect2";
import { createArray, assert } from "./utils";
import { Graph } from "./Graph";
import { Pathfinder } from "./Pathfinder";

export class DebugInfo {
	private labels: Rect2[] = [];
	private graphs: Graph[] = [];

	matrix: number[];

	addLabel(rect: Rect2) {
		this.labels.push(rect);
	}

	getLabels(): Rect2[] {
		return this.labels;
	}

	addGraph(graph: Graph) {
		this.graphs.push(graph);
	}

	getGraphs(): Graph[] {
		return this.graphs;
	}
};

export class Map {
	private floors: number[];
	private walls_h: number[];
	private walls_v: number[];

	readonly debug: DebugInfo = new DebugInfo();
	readonly width: number;
	readonly height: number;

	constructor(width: number, height: number) {
		this.width = width;
		this.height = height;

		this.floors = createArray<number>(width * height, 0);
		this.walls_h = createArray<number>(width * (height + 1), 0);
		this.walls_v = createArray<number>((width + 1) * height, 0);
	}

	getFloor(pos: Vector2): number {
		pos = pos.clone().floor();

		const idx = pos.x + pos.y * this.width;
		assert(idx >= 0 && idx < this.width * this.height);
		return this.floors[idx];
	}

	setFloor(pos: Vector2, v: number) {
		pos = pos.clone().floor();

		const idx = pos.x + pos.y * this.width;
		assert(idx >= 0 && idx < this.width * this.height);
		this.floors[idx] = v;
	}

	getHorizontalWall(pos: Vector2): number {
		pos = pos.clone().floor();

		const idx = pos.x + pos.y * this.width;
		assert(pos.x >= 0 && pos.y >= 0);
		assert(pos.x < this.width && pos.y < this.height + 1);
		return this.walls_h[idx];
	}

	setHorizontalWall(pos: Vector2, v: number) {
		pos = pos.clone().floor();

		const idx = pos.x + pos.y * this.width;
		assert(pos.x >= 0 && pos.y >= 0);
		assert(pos.x <= this.width && pos.y <= this.height + 1);
		this.walls_h[idx] = v;
	}

	getVerticalWall(pos: Vector2): number {
		pos = pos.clone().floor();

		const idx = pos.x + pos.y * this.width;
		assert(pos.x >= 0 && pos.y >= 0);
		assert(pos.x < this.width + 1 && pos.y < this.height);
		return this.walls_v[idx];
	}

	setVerticalWall(pos: Vector2, v: number) {
		pos = pos.clone().floor();

		const idx = pos.x + pos.y * this.width;
		assert(pos.x >= 0 && pos.y >= 0);
		assert(pos.x < this.width + 1 && pos.y < this.height);
		assert(idx >= 0 && idx < (this.width + 1) * this.height);
		this.walls_v[idx] = v;
	}

	fillFloor(rect: Rect2, value: number) {
		for (let y = rect.minPos.y; y < rect.maxPos.y; y++) {
			for (let x = rect.minPos.x; x < rect.maxPos.x; x++) {
				const idx = x + y * this.width;
				this.floors[idx] = value;
			}
		}
	}

	placeWallRect(rect: Rect2, value: number) {
		for (let x = rect.minPos.x; x < rect.maxPos.x; x++) {
			this.walls_h[x + rect.minPos.y * this.width] = value;
			this.walls_h[x + rect.maxPos.y * this.width] = value;
		}

		for (let y = rect.minPos.y; y < rect.maxPos.y; y++) {
			this.walls_v[rect.minPos.x + y * this.width] = value;
			this.walls_v[rect.maxPos.x + y * this.width] = value;
		}
	}

	placeHallway(start: Vector2, end: Vector2) {
		if (start.y == end.y) {
			if (start.x > end.x) {
				const tmp = start;
				start = end;
				end = tmp;
			}

			for (let x = start.x; x <= end.x; x++) {
				const pos = new Vector2(x, start.y);
				if (this.getFloor(pos) == 0) {
					this.setFloor(pos, 2);

					if (this.getFloor(new Vector2(x, start.y - 1)) == 0) {
						this.setHorizontalWall(new Vector2(x, start.y), 1);
					} else {
						this.setHorizontalWall(new Vector2(x, start.y), 0);
					}
					if (this.getFloor(new Vector2(x, start.y + 1)) == 0) {
						this.setHorizontalWall(new Vector2(x, start.y + 1), 1);
					} else {
						this.setHorizontalWall(new Vector2(x, start.y + 1), 0);
					}

					this.setVerticalWall(new Vector2(x, start.y), 0);
					this.setVerticalWall(new Vector2(x + 1, start.y), 0);
				}
			}

			if (this.getFloor(start.clone().add(new Vector2(-1, 0))) == 0) {
				this.setVerticalWall(start, 1);
			}

			const endWall = end.clone().add(new Vector2(1, 0));
			if (this.getFloor(endWall) == 0) {
				this.setVerticalWall(endWall, 1);
			}
		} else if (start.x == end.x) {
			if (start.y > end.y) {
				const tmp = start;
				start = end;
				end = tmp;
			}

			for (let y = start.y; y < end.y; y++) {
				const pos = new Vector2(start.x, y);
				if (this.getFloor(pos) == 0) {
					this.setFloor(pos, 2);

					if (this.getFloor(new Vector2(start.x - 1, y)) == 0) {
						this.setVerticalWall(new Vector2(start.x, y), 1);
					} else {
						this.setVerticalWall(new Vector2(start.x, y), 0);
					}
					if (this.getFloor(new Vector2(start.x + 1, y )) == 0) {
						this.setVerticalWall(new Vector2(start.x + 1, y), 1);
					} else {
						this.setVerticalWall(new Vector2(start.x + 1, y), 0);
					}

					this.setHorizontalWall(new Vector2(start.x, y), 0);
					this.setHorizontalWall(new Vector2(start.x, y + 1), 0);
				}
			}

			if (this.getFloor(start.clone().add(new Vector2(0, -1))) == 0) {
				this.setHorizontalWall(start, 1);
			}

			const endWall = end.clone().add(new Vector2(0, 1));
			if (this.getFloor(endWall) == 0) {
				this.setHorizontalWall(endWall, 1);
			}
		} else {
			throw "Invalid hallway";
		}
	}

	getHallwayCollision(start: Vector2, end: Vector2): Vector2 {
		assert(start.x >= 0 && start.x < this.width && start.y >= 0 && start.y < this.height);
		assert(end.x >= 0 && end.x < this.width && end.y >= 0 && end.y < this.height);

		const direction = end.clone().subtract(start).normalise();
		if ((direction.x == 0 && direction.y == 0) || (direction.x != 0 && direction.y != 0)) {
			throw "Invalid hallway";
		}

		for (let pos = start.clone(); !pos.equals(end); pos.add(direction)) {
			if (this.getFloor(pos) != 0) {
				return pos;
			}
		}

		return null
	}

	placeHallways(input: Graph, rooms: Rect2[]): Graph {
		const grid = createArray(this.width * this.height, 8);
		rooms.forEach(room => {
			for (let x = room.minPos.x - 3; x < room.maxPos.x + 3; x++) {
				for (let y = room.minPos.y - 3; y < room.maxPos.y + 3; y++) {
					const idx = x + y * this.width;
					grid[idx] = 8;
				}
			}
		});
		rooms.forEach(room => {
			for (let x = room.minPos.x - 1; x < room.maxPos.x + 1; x++) {
				for (let y = room.minPos.y - 1; y < room.maxPos.y + 1; y++) {
					const idx = x + y * this.width;
					grid[idx] = 16;
				}
			}
		});
		rooms.forEach(room => {
			for (let x = room.minPos.x; x < room.maxPos.x; x++) {
				for (let y = room.minPos.y; y < room.maxPos.y; y++) {
					const idx = x + y * this.width;
					grid[idx] = 4;
				}
			}
		});
		rooms.forEach(room => {
			const sides = [
				new Vector2(room.minPos.x - 1, room.center.y).round(),
				new Vector2(room.center.x, room.minPos.y - 1).round(),
			];

			sides.forEach(side => {
				const idx = side.x + side.y * this.width;
				grid[idx] *= 0.95;
			})
		});

		this.debug.matrix = grid;

		const pf = new Pathfinder(this.width, this.height, grid);

		const graph = new Graph();
		const dummy = input.clone();

		dummy.getEdges()
			.forEach(edge => {
				if (dummy.hasDirectedEdge(edge.from, edge.to)) {
					dummy.disconnect(edge.from, edge.to);

					const fromRoom = edge.from.bounds;
					const toRoom = edge.to.bounds;

					const path = pf.findPath(fromRoom.center.round(), toRoom.center.round());
					for (let i = 1; i < path.length; i++) {
						const pos = path[i]
						this.placeHallway(path[i - 1], pos);

						const idx = pos.x + pos.y * this.width;
						if (grid[idx] > 1) {
							grid[idx] = 1;
						}
					}
				}
			});


		return graph;
	}

};
