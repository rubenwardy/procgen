import { Map } from "./Map";
import { randomPlaceGenerator } from "./RandomPlaceGenerator";
import { Vector2 } from "./Vector2";
import { Edge, Graph } from "./Graph";
import { growingTreeGenerator } from "./GrowingTreeGenerator";
const buttonbarTemplate = require("./../templates/buttonbar.handlebars")

type Generator = (map: Map, seed: String) => void;
export class Game {
	canvas_element: HTMLCanvasElement;
	infotext: HTMLElement;
	mousePosition: Vector2;

	generators: Generator[] = [ growingTreeGenerator, randomPlaceGenerator ];
	generatorNames: string[] = [ "Growing Tree", "Random Placement" ];

	generatorId: number;

	get generator(): Generator {
		console.log(`Using generator ${this.generatorName}`);
		return this.generators[this.generatorId];
	}

	get generatorName(): string {
		return this.generatorNames[this.generatorId];
	}

	map: Map;
	seed: string;
	showDebug: boolean = true;

	constructor(canvas_element: HTMLCanvasElement) {
		this.canvas_element = canvas_element;
		this.showDebug = window.localStorage.getItem("showDebug") == "true";
		this.generatorId = parseInt(window.localStorage.getItem("generator")) || 0;

		this.generate();

		this.createButtonBar();

		this.infotext = document.createElement("span");
		this.infotext.style.position = "fixed";
		this.infotext.style.top = "10px";
		this.infotext.style.right = "10px";
		this.infotext.style.color = "white";
		this.infotext.style.padding = "5px";
		this.infotext.style.backgroundColor = "rgba(0, 0, 0, 0.75)";
		this.infotext.innerText = "Ready";
		document.getElementById("hud").appendChild(this.infotext);

		document.onmousemove = ev => {
			this.mousePosition = new Vector2(ev.clientX, ev.clientY);
		};
	}

	createButtonBar() {
		let ele = document.createElement("div");
		ele.innerHTML = buttonbarTemplate([
			{ "id": "new", "text": "New Dungeon" },
			{ "id": "regen", "text": "Regenerate" },
			{ "id": "save", "text": "Fix Seed" },
			{ "id": "toggle", "text": "Toggle Debug" },
			{ "id": "nextgen", "text": "Next Generator" },
		]);

		const buttonbar = <HTMLElement>ele.children[0];
		document.getElementById("hud").appendChild(buttonbar);

		const saveSeedBtn = document.getElementById("save");
		function updateSaveSeedBtn() {
			if (window.localStorage.getItem("seed")) {
				saveSeedBtn.innerText = "Clear Seed";
			} else {
				saveSeedBtn.innerText = "Fix Seed";
			}
		}
		updateSaveSeedBtn();

		document.getElementById("new").addEventListener("click",
				() => {
					this.resetSavedSeed();
					updateSaveSeedBtn();
					this.generate();
				});

		document.getElementById("regen").addEventListener("click",
				() => {
					this.saveSeed();
					updateSaveSeedBtn();
					this.generate();
				});

		saveSeedBtn.addEventListener("click",
				() => {
					if (window.localStorage.getItem("seed")) {
						this.resetSavedSeed();
					} else {
						this.saveSeed();
					}
					updateSaveSeedBtn();
				});

		document.getElementById("toggle").addEventListener("click",
				() => {
					this.showDebug = !this.showDebug;
					window.localStorage.setItem("showDebug", this.showDebug ? "true" : "false");
				});


		const nextGenButton = document.getElementById("nextgen");

		const updateNextGenBtn = () => {
			nextGenButton.innerText = `Algorithm: ${this.generatorName}`;
		}
		updateNextGenBtn();

		nextGenButton.addEventListener("click",
				() => {
					this.generatorId += 1;
					if (this.generatorId >= this.generators.length) {
						this.generatorId = 0;
					}

					window.localStorage.setItem("generator", this.generatorId.toString());

					this.saveSeed();
					updateSaveSeedBtn();
					updateNextGenBtn();
					this.generate();
				});
	}

	generateSeed(): string {
		let seed = window.localStorage.getItem("seed");
		if (seed) {
			console.log(`Using saved seed: ${seed}`);
		} else {
			seed = Math.random().toString(36).substring(2, 15)
			console.log(`Using new seed: ${seed}`);
		}
		return seed;
	}

	resetSavedSeed() {
		window.localStorage.removeItem("seed");
	}

	saveSeed() {
		window.localStorage.setItem("seed", this.seed);
	}

	generate() {
		console.log("%c\n========== GENERATING ==========", "font-size: 120%; margin: 20px 0;");

		this.map = new Map(50, 50);
		this.seed = this.generateSeed();

		// try {
			this.generator(this.map, this.seed);
		// } catch (x) {
		// 	console.log("Error in generator:", x);
		// }
	}

	draw() {
		const window_size = new Vector2(this.canvas_element.width, this.canvas_element.height).immutable();
		const screen_sidelen = Math.min(window_size.x, window_size.y);
		const map_sidelen = Math.max(this.map.width, this.map.height);
		const tilesize = Math.floor((screen_sidelen - 15) / map_sidelen);
		const size = new Vector2(tilesize * this.map.width, tilesize * this.map.height).immutable();
		const origin = window_size.clone().subtract(size).divide(2).floor().immutable();

		const c = this.canvas_element.getContext("2d");
		c.fillStyle = "white";
		c.fillRect(origin.x, origin.y, size.x, size.y);

		function project(point: Vector2): Vector2 {
			return point.clone().multiply(tilesize).add(origin).floor();
		}

		function projectInverse(point: Vector2): Vector2 {
			return point.clone().subtract(origin).divide(tilesize).floor();
		}

		const mousePos = this.mousePosition;
		if (mousePos) {
			const mouseTile = projectInverse(mousePos);
			this.infotext.innerText = `Mouse: ${mouseTile.toString()}`;
		} else {
			this.infotext.innerText = "Ready";
		}

		// Draw floor
		for (let y = 0; y < this.map.height; y++) {
			for (let x = 0; x < this.map.width; x++) {
				const pos = new Vector2(x, y);
				const value = this.map.getFloor(pos);
				if (value != 0) {
					const point = project(pos);
					c.fillStyle = `rgba(0, 0, 0, ${(value) * 0.1})`;
					c.fillRect(point.x, point.y, tilesize, tilesize);
				}
			}
		}

		// Draw Grid
		c.beginPath();
		c.setLineDash([2, 2]);
		c.lineWidth = 1;
		c.strokeStyle = "rgba(0, 0, 0, 0.2)";
		for (let x = 0; x < this.map.width; x++) {
			const from = project(new Vector2(x, 0));
			const to = project(new Vector2(x, this.map.height));
			c.moveTo(from.x, from.y);
			c.lineTo(to.x, to.y);
		}
		for (let y = 0; y < this.map.height; y++) {
			const from = project(new Vector2(0, y));
			const to = project(new Vector2(this.map.width, y));
			c.moveTo(from.x, from.y);
			c.lineTo(to.x, to.y);
		}
		c.stroke();

		// Draw walls
		c.beginPath();
		c.setLineDash([]);
		c.lineWidth = 3;
		c.strokeStyle = "black";
		for (let y = 0; y < this.map.height + 1; y++) {
			for (let x = 0; x < this.map.width; x++) {
				const value = this.map.getHorizontalWall(new Vector2(x, y));
				if (value != 0) {
					const from = project(new Vector2(x, y));
					const to = project(new Vector2(x + 1, y));

					c.moveTo(from.x, from.y);
					c.lineTo(to.x, to.y);
				}
			}
		}

		for (let y = 0; y < this.map.height; y++) {
			for (let x = 0; x < this.map.width + 1; x++) {
				const value = this.map.getVerticalWall(new Vector2(x, y));
				if (value != 0) {
					const from = project(new Vector2(x, y));
					const to = project(new Vector2(x, y + 1));

					c.moveTo(from.x, from.y);
					c.lineTo(to.x, to.y);
				}
			}
		}
		c.stroke();

		function drawGraph(graph: Graph) {
			// Draw Graph
			c.beginPath();

			graph.getEdges().forEach((edge: Edge) => {
				const from = project(edge.from.pos);
				const to = project(edge.to.pos);

				c.moveTo(from.x, from.y);
				c.lineTo(to.x, to.y);
			});
			c.stroke();
		}

		if (this.showDebug) {
			const colors = [
				"#336699",
				"green"
			];

			const graphs = this.map.debug.getGraphs();
			c.setLineDash([]);
			for (let i = 0; i < graphs.length; i++) {
				c.lineWidth = i * 2 + 1;
				c.strokeStyle = colors[i];
				drawGraph(graphs[i]);
			}

			c.font = "12px Arial";
			c.fillStyle = "black";
			this.map.debug.getLabels().forEach(label => {
				const pos = project(label.center);
				c.fillText(label.name, pos.x, pos.y);
			});

			// if (this.map.debug.matrix) {
			// 	c.font = "8px Arial";
			// 	c.fillStyle = "black";
			// 	c.textAlign = "center";
			// 	c.textBaseline = "middle";
			// 	for (let y = 0; y < this.map.height; y++) {
			// 		for (let x = 0; x < this.map.width + 1; x++) {
			// 			const pos = project(new Vector2(x + 0.5, y + 0.5));
			// 			const idx = x + y * this.map.width;
			// 			const text = Math.round(this.map.debug.matrix[idx]).toString();
			// 			c.fillText(text, pos.x, pos.y);
			// 		}
			// 	}
			// }
		}
	}
};
