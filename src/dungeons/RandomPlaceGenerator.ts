import { Map } from "./Map";
import { Rect2 } from "./Rect2";
import { Vector2 } from "./Vector2";
import { Graph } from "./Graph";
import { PRNGRandom } from "./utils";

let rng : PRNGRandom;

function roomOverlaps(rooms: Rect2[], room: Rect2): boolean {
	const bounds = room.grow(1);
	return rooms.some(x => x.intersects(bounds));
}

function generateRooms(map: Map, number: number): Rect2[] {
	let rooms: Rect2[] = [];
	let attempts = 0;

	while (rooms.length < number) {
		if (attempts > number * 1000) {
			console.log(`Ran out of attempts, only generated ${rooms.length}/${number} rooms`);
			break;
		}

		const halfSize = new Vector2(map.width / 2, map.height / 2);
		const pos = rng.getRandomPointInCircle(halfSize, halfSize.clone().multiply(0.8));
		const size = rng.randomSize(11, 2.5);
		const room = new Rect2(pos, size, `Room ${rooms.length + 1}`).round();

		if (!roomOverlaps(rooms, room) &&
				room.maxPos.x < map.width &&
				room.maxPos.y < map.height) {
			rooms.push(room);
		}

		attempts++;
	}

	return rooms;
}

function placeRooms(map: Map, rooms: Rect2[]) {
	rooms.forEach(room => {
		map.debug.addLabel(room);
		map.fillFloor(room, 1);
		map.placeWallRect(room, 1);
	});
}

function filterRooms(rooms: Rect2[]): Rect2[] {
	return rooms.filter(room => {
		return room.size.x * room.size.y > 30;
	});
}

export function randomPlaceGenerator(map: Map, seed: string) {
	rng = new PRNGRandom(seed);

	let graph = new Graph();

	filterRooms(generateRooms(map, 30))
			.forEach(room => graph.addRectNode(room));

	let delaunayGraph = graph.delaunay();
	delaunayGraph.reposition(map.width, map.height);

	map.debug.addGraph(delaunayGraph);

	let mstGraph = delaunayGraph.minimumSpanningTree();
	map.debug.addGraph(mstGraph);

	const fullEdges = delaunayGraph.getEdges();
	const mstEdges = mstGraph.getEdges();
	const targetAddBack = Math.ceil((fullEdges.length - mstEdges.length) * 0.2);
	for (let i = 0; i < targetAddBack; i++) {
		for (let attempt = 0; attempt < 100; attempt++) {
			const edge = rng.randomElement(fullEdges);
			if (!mstGraph.hasDirectedEdge(edge.from, edge.to)) {
				mstGraph.connect(edge.from, edge.to);
				break;
			}
		}
	}

	const rooms = mstGraph.getNodes().map(node => node.bounds);
	placeRooms(map, rooms);
	map.placeHallways(mstGraph, rooms);
}
