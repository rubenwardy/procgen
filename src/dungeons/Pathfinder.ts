import { Vector2 } from "./Vector2";
import { assert } from "./utils";
import TinyQueue from "tinyqueue";

class Node {
	readonly pos: Vector2;

	parent: Node;
	weightToHere: number;
	readonly distanceToEnd: number;

	get totalWeight(): number {
		return this.weightToHere + this.distanceToEnd;
	}

	constructor(pos: Vector2, parent: Node, weightToHere: number, distanceToEnd: number) {
		this.pos = pos;
		this.parent = parent;
		this.weightToHere = weightToHere;
		this.distanceToEnd = distanceToEnd;
	}

	update(parent: Node, weightToHere: number) {
		this.parent = parent;
		this.weightToHere = weightToHere;
	}
}

export class Pathfinder {
	private weights: number[];
	private width: number;
	private height: number;

	constructor(width: number, height: number, grid: number[]) {
		this.width = width;
		this.height = height;
		this.weights = grid;
	}

	private getWeight(pos: Vector2) {
		assert(pos.x >= 0 && pos.x < this.width && pos.y >= 0 && pos.y < this.height);

		const idx = pos.x + pos.y * this.width;
		return this.weights[idx];
	}

	findPath(from: Vector2, to: Vector2): Vector2[] {
		// console.log(`Pathfinding from ${from.toString()} to ${to.toString()}`);

		let queue = new TinyQueue<Node>([], (a, b) => a.totalWeight - b.totalWeight);
		let nodes = new Map<number, Node>();
		let closedSet = new Set<number>();

		{
			const start = new Node(from, null, 0, from.sqdist(to));
			queue.push(start);
			nodes.set(from.hash(), start);
		}

		let attempts = 0;

		while (queue.length > 0) {
			attempts++;

			const node = queue.pop();
			// console.log(` - Evaluating node at ${node.pos.toString()}. ${node.totalWeight}=${node.weightToHere}+${node.distanceToEnd}`);
			closedSet.add(node.pos.hash());

			if (attempts > 1000) {
				throw "Error";
			}

			if (node.distanceToEnd == 0) {
				let path: Vector2[] = [];

				let current = node;
				while (current != null) {
					path.push(current.pos);

					current = current.parent;
				}

				return path.reverse();
			}

			const neighbours = [
				new Vector2(0, -1),
				new Vector2(0, 1),
				new Vector2(-1, 0),
				new Vector2(1, 0)
			]

			neighbours.forEach(direction => {
				const newPos = node.pos.clone().add(direction);
				if (newPos.x < 0 || newPos.x >= this.width ||
						newPos.y < 0 || newPos.y >= this.height) {
					return;
				}

				if (closedSet.has(newPos.hash())) {
					return;
				}

				let weight = node.weightToHere + this.getWeight(newPos);

				if (node.parent) {
					const previousDirection = node.pos.clone().subtract(node.parent.pos);
					if (previousDirection.x != direction.x || previousDirection.y != direction.y) {
						weight *= 1.1;
					}
				}

				let oldNode = nodes.get(newPos.hash());
				if (oldNode == null) {
					const newNode = new Node(newPos, node, weight, newPos.sqdist(to));
					queue.push(newNode);
					nodes.set(from.hash(), newNode);
				} else if (oldNode.weightToHere <= weight) {
					oldNode.update(node, weight);
				}
			});
		}

		return [ from, to ];
	}
}
