import { Game } from "./Game";

declare global {
	interface Window { game: Game; }
}

window.onload = () => {
	let canvas_element = <HTMLCanvasElement>document.getElementById("canvas");
	canvas_element.width = window.innerWidth;
	canvas_element.height = window.innerHeight;

	let game = new Game(canvas_element);
	window.game = game;

	let callback = () => {
		game.draw();
		window.requestAnimationFrame(callback);
	};

	window.onresize = () => {
		canvas_element.width = window.innerWidth;
		canvas_element.height = window.innerHeight;
	};

	window.requestAnimationFrame(callback);
};
